/*======================================================================================================
 * All Claims details including vehicle info
========================================================================================================*/
//Get all claims
function getClaims(){
	WL.Logger.debug("Calling SQL proc getClaimsData with " );
	return WL.Server.invokeSQLStoredProcedure({
		procedure : "getAllClaims",
		parameters : []
	});
}
//not used now...
function getClaimDetails(claimId){
	WL.Logger.debug("Calling SQL proc getClaimsDetails with " + claimId);
	return WL.Server.invokeSQLStoredProcedure({
		procedure : "getClaimDetailsByClaimId",
		parameters : [claimId]
	});
}

/*======================================================================================================
 * Vehicle Claim Details
========================================================================================================*/

//Vehicle Info Update
var infoStatement1 = "UPDATE dbo.VehicleInfo SET NumberOfDoors = ?, Odometer = ? , Colour = ?, EngineNo = ?, " +
		"BodyType = ?, ChassisNo = ?, RegistrationExpiry = ? WHERE ClaimId = ?";	
var infoPreparedStatement1 = WL.Server.createSQLStatement(infoStatement1);

function updateVehicleDetails (queryData, claimId){	
	
	WL.Logger.debug ("Begin update of vehicle info");
	WL.Logger.debug ("claimId = " + claimId);
		
	WL.Logger.debug ("statement is " + infoPreparedStatement1);
	return WL.Server.invokeSQLStatement({
		preparedStatement : infoPreparedStatement1,
		parameters : [ queryData.NumberOfDoors, queryData.Odometer, queryData.Colour, queryData.EngineNo, 
		              queryData.BodyType, queryData.ChassisNo, queryData.RegistrationExpiry, claimId]
	});
}

/*======================================================================================================
 * Vehicle Condition details
========================================================================================================*/

//Get Vehicle Condition details
function getVehicleConditionDetails(claimId){
	WL.Logger.debug("Calling SQL proc getVehicleConditionDetails with " + claimId);
	return WL.Server.invokeSQLStoredProcedure({
		procedure : "getVehicleConditionDetails",
		parameters : [claimId]
	});
}

//Vehicle Condition Info Update 
var condStatement1 = "UPDATE dbo.VehicleCondition SET PreAccidentValue = ?, ApproxSalvageValue = ?, SalvageRefNo = ?," +
		"TyreTreadRHF = ? , TyreTreadRHR = ?, TyreTreadLHF = ?, TyreTreadLHR = ? WHERE ClaimId = ?";	
var condPreparedStatement1 = WL.Server.createSQLStatement(condStatement1);

function updateVehicleConditionDetails (queryData, claimId){	
	
	WL.Logger.debug ("Begin update of vehicle condition info");
	WL.Logger.debug ("claimId = " + claimId);
		
	WL.Logger.debug ("statement is " + condPreparedStatement1);
	return WL.Server.invokeSQLStatement({
		preparedStatement : condPreparedStatement1,
		parameters : [ queryData.PreAccidentValue, queryData.ApproxSalvageValue, queryData.SalvageRefNo, queryData.TyreTreadRHF, 
		              queryData.TyreTreadRHR, queryData.TyreTreadLHF, queryData.TyreTreadLHR, claimId]
	});
}

/*======================================================================================================
 * Vehicle Modifications
========================================================================================================*/

//Get Vehicle Mods
var modStatement1 = "SELECT * from dbo.VehicleModification WHERE ClaimId = ?";	
var modPreparedStatement1 = WL.Server.createSQLStatement(modStatement1);

function getVehicleMods (claimId){	
	
	WL.Logger.debug ("claimId = " + claimId);		
	WL.Logger.debug ("statement is " + modPreparedStatement1);
	
	return WL.Server.invokeSQLStatement({
		preparedStatement : modPreparedStatement1,
		parameters : [claimId]
	});
}

//Vehicle Modification Insert 
function insertVehicleMods (ClaimId, Title, Description){	
	
	WL.Logger.debug("Calling SQL proc insertVehicleMods "  );
	WL.Logger.debug("Claim Id - " + ClaimId );
	WL.Logger.debug("Title - " + Title);
	WL.Logger.debug("Description - " + Description );
	
	return WL.Server.invokeSQLStoredProcedure({
		procedure : "insertModData",
		parameters : [ClaimId, Title, Description]
	});
}

//Vehicle Modification Update 
var modStatement2 = "UPDATE dbo.VehicleModification SET Title = ?, Description = ? WHERE ModId = ?";	
var modPreparedStatement2 = WL.Server.createSQLStatement(modStatement2);

function updateVehicleMods ( Title, Description, modId){	
	
	WL.Logger.debug ("Begin update of vehicle mods");
	WL.Logger.debug ("modId = " + modId);
		
	WL.Logger.debug ("statement is " + modPreparedStatement2);
	return WL.Server.invokeSQLStatement({
		preparedStatement : modPreparedStatement2,
		parameters : [ Title, Description, modId]
	});
}

//Delete Modification item
var modStatement3 = "DELETE from dbo.VehicleModification WHERE ModId = ?";	
var modPreparedStatement3 = WL.Server.createSQLStatement(modStatement3);

function deleteVehicleMod ( modId){	
	
	WL.Logger.debug ("Begin delete of vehicle mod");
	WL.Logger.debug ("modId = " + modId);
		
	WL.Logger.debug ("statement is " + modPreparedStatement3);
	return WL.Server.invokeSQLStatement({
		preparedStatement : modPreparedStatement3,
		parameters : [  modId]
	});
}


//Damage Assessment

//Get pictures
var dStatement1 = "SELECT * from dbo.DamagePhotos WHERE ClaimId = ?";	
var dPreparedStatement1 = WL.Server.createSQLStatement(dStatement1);

function getPhotos (claimId){	
	
	WL.Logger.debug ("claimId = " + claimId);		
	WL.Logger.debug ("statement is " + dPreparedStatement1);
	
	return WL.Server.invokeSQLStatement({
		preparedStatement : dPreparedStatement1,
		parameters : [claimId]
	});
}

//Insert Picture
function insertPhoto (ClaimId, imageData){	
	
	WL.Logger.debug("Calling SQL proc insertPhoto "  );
	WL.Logger.debug("Claim Id - " + ClaimId );
	
	
	return WL.Server.invokeSQLStoredProcedure({
		procedure : "insertDamagePhoto",
		parameters : [ClaimId, imageData]
	});
}

//Delete photo

function deletePhoto ( photoId){	
	
	WL.Logger.debug ("Calling stored proc deletePhotoDamages for " +  photoId);
			
	return  WL.Server.invokeSQLStoredProcedure({
		procedure : "deletePhotoDamages",
		parameters : [photoId]
	});
}

//Get damage modifications
var cStatement1 = "SELECT CoordinateX, CoordinateY from dbo.DamageCoordinates WHERE DamageId = ? ";	
var cPreparedStatement1 = WL.Server.createSQLStatement(cStatement1);
function getDamages (photoId){	
	
	WL.Logger.debug ("PhotoId = " + photoId);		
		
	var result =  WL.Server.invokeSQLStoredProcedure({
		procedure : "getDamages",
		parameters : [photoId]
	});
	//WL.Logger.debug ("result coords = " + result.resultSet[0].CoordinateX);
	var c_result;
	if (result.resultSet.length > 0){
		for (var i=0;i<result.resultSet.length;i++){
			var d_id = result.resultSet[i].DamageId;
			
			c_result =  WL.Server.invokeSQLStatement({
				preparedStatement : cPreparedStatement1,
				parameters : [d_id]
			});
			result.resultSet[i].Coords = c_result.resultSet;
		}
	}
	return result;
}

//Insert Damage
function insertDamage (queryData, photoId){	
	
	WL.Logger.debug("Calling SQL proc insertDamage "  );
	WL.Logger.debug("PhotoId - " + photoId );
	WL.Logger.debug("TYpe - " + queryData.Type );
	
	
	//insert into dbo.Damages and get the damageId

	var result =  WL.Server.invokeSQLStoredProcedure({
		procedure : "insertDamage",
		parameters : [photoId, queryData.Type , queryData.DrawColor]
	});
	if (result.resultSet.length > 0){
		var damageId = result.resultSet[0].DamageId;
		WL.Logger.debug("DamageID = " + damageId);
		
		insertDamageCoords (queryData.Coords, damageId);
		
	}
	
	return result;	
	
}

var dStatement3 = "INSERT into dbo.DamageCoordinates VALUES (?,?,?)";
var dPreparedStatement3 = WL.Server.createSQLStatement(dStatement3);
function insertDamageCoords (coordsArr, damageId){
	var result;
	for (var i=0;i<coordsArr.length;i += 1){
		result = WL.Server.invokeSQLStatement({
			preparedStatement : dPreparedStatement3,
			parameters : [ damageId, coordsArr[i].CoordinateX,coordsArr[i].CoordinateY]
		});
	}
	 return result;
}

function updateDamage (queryData, damageId){
	
	var result;
	var coordsArr = queryData.Coords;
	for (var i=0;i<coordsArr.length;i += 1){
		updateDamageCoordinates (damageId, coordsArr[i].CoordinateX,coordsArr[i].CoordinateY);
	}
	 return result;
}
//update damage coordinates
var damageStatement1 = "UPDATE dbo.DamageCoordinates SET CoordinateX = ?, CoordinateY = ? WHERE DamageId = ?";	
var damagePreparedStatement1 = WL.Server.createSQLStatement(damageStatement1);

function updateDamageCoordinates ( damageId,x,y){	
	
	WL.Logger.debug ("Begin update of damage coordinates");
	WL.Logger.debug ("damageId = " + damageId);
		
	WL.Logger.debug ("statement is " + damagePreparedStatement1);
	return WL.Server.invokeSQLStatement({
		preparedStatement : damagePreparedStatement1,
		parameters : [ x,y,damageId]
	});
}
