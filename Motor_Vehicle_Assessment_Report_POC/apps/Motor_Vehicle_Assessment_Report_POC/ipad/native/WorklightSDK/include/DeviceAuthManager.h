/*
 * Licensed Materials - Property of IBM
 * 5725-G92 (C) Copyright IBM Corp. 2006, 2012. All Rights Reserved.
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
 */

//
//  DeviceAuthManager.h
//  WorklightStaticLibProject
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonDigest.h>
#import <Cordova/CDVPlugin.h>
#import "ProvisioningDelegateImpl.h"


@interface DeviceAuthManager : NSObject

/**
 * Get the DeviceAuthManager singleton instance 
 */
+ (DeviceAuthManager *) sharedInstance;

/**
 * Generate KeyPair (private + public) and save it to Key Chain on the device.
 * Using RSA 512 long key size
 */
-(void) generateKeyPair;

/**
 * return the realm name from the 401 challenge
 */
-(NSString *) getRealmName;

/**
 * Add basic data provisioning against a worklight server based on provisioning entity(application/group/device).
 * For all provisioning entity types - add deviceId parameter.
 * provisioning entity == application, add application parameter.
 * provisioning entity == group, add groupId parameter.
 * payload - custom payload. *
 * return - NSMutableDictionary contains basic and custom payload.
 */
-(NSMutableDictionary *) addBasicDeviceProvisioningProperties: (NSMutableDictionary *)payload;

/**
 * This method signs on a given content according to JSW standard.
 * We'll using the public key
 * Sign the header and payload with SHA256 / RSA 512 
 * csrPayload- NSMutableDictionary with the content sign on.
 * return - the signed string.
 */
-(NSString *) createCsrHeader:(NSMutableDictionary *)csrPayload;

/**
 * Entry point for ProvisioningDelegate to save the recieved certificate to the keystore.
 * When finished saving, it will start the device authentication process.
 * keyPair
 * certificate - NSData represent the certificate
 */
-(void) saveCertificate:(NSData *)certificateData;

/**
 * Called when failed to create a certificate, will show an error message to the client, and close the application
 * If the user implemented its own provider, then he MUST call this function when getting a failure to get a certificate.
 */
-(void)csrCertificateRecieveFailed;


//Call this initializer only
-(DeviceAuthManager *) init:(NSString *)granularity isProvisioning:(BOOL)isProvisioning 
isProvisioningAllowed:(BOOL)isProvisioningAllowed pluginForJSExec:(CDVPlugin *) plugin provisioningRealm:(NSString *) realm;
-(NSMutableDictionary *) addDeviceIdAndAppId:(NSMutableDictionary *) payloadJSON;
-(BOOL) isCertificateExist;
-(NSData *)getKeyChainKeyBits:(NSData *) keychainTag isCertificate:(BOOL) isCertificate;
-(NSString *) getWLUniqueDeviceId;
-(NSData *) signData:(NSString *)paylaod privateKey:(SecKeyRef)privateKey;
-(void) setProvisioningDelegate:(id <ProvisioningDelegate>) pDelegate;
-(id <ProvisioningDelegate>) getProvisioningDelegate;
-(void)createCsr:(NSMutableDictionary *) csrPayload;
-(NSData *) getKeyIdentifier :(BOOL) isPublic;
@end

