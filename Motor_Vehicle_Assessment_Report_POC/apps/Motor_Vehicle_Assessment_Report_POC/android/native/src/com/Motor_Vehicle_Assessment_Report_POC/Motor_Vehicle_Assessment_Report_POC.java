package com.Motor_Vehicle_Assessment_Report_POC;

import android.os.Bundle;

import com.worklight.androidgap.WLDroidGap;

public class Motor_Vehicle_Assessment_Report_POC extends WLDroidGap {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		   
		   if(android.os.Build.VERSION.SDK_INT > 15) {
			   this.appView.getSettings().setAllowUniversalAccessFromFileURLs(true);
		   }
		   
		   //DeviceAuthManager.getInstance().setProvisioningDelegate(<Use default ProvisioningDelegateImpl class or replace with your IProvisioningDelegate implementation>);
		   super.loadUrl(getWebMainFilePath());
	}		
}




