
/* JavaScript content from js/claimsViewModel_customBindings.js in folder common */

/*Binding Handlers :
 * element � The DOM element involved in this binding
 * valueAccessor � Js function to get the current model property that is involved in this binding. 
 * allBindingsAccessor � Js function to get all the model properties bound to this DOM element. 
 *  					calling without any parameters will get the current bound model properties.
 * viewModel � The view model object that was passed to ko.applyBindings.
 * bindingContext � An object that holds the binding context available to this element�s bindings. 
 * 					This object includes special properties including $parent, $parents, and $root that 
 * 					can be used to access data that is bound against ancestors of this context.
 */ 

//This binding is used instead of with binding to trigger the jqm styles

ko.bindingHandlers['mobileWith'] = {

	    'init': function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
	        return ko.bindingHandlers["with"].init(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);
	    },
	    'update': function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
	        var t = ko.bindingHandlers["with"].update(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);
	        setTimeout(function () {
	        	//$(element).listview('refresh');
	            $(element).trigger('pagecreate'); //fixes un-enhanced issues
	        }, 0);
	        return t;
	    }

	};

//to refresh the list view
ko.bindingHandlers['mobileList'] = {
	    'update': function (element, valueAccessor) {
	    	 setTimeout(function () { //To make sure the refresh fires after the DOM is updated
	    	        console.log("listview refresh");
	    	        $(element).listview('refresh');
	    	    }, 0);

	    }
	};

//to check if value is true or false and return yes or no instead
ko.bindingHandlers['mobileBoolean'] = {
	    'update': function (element, valueAccessor) {
	    	 
	    	var value = ko.utils.unwrapObservable(valueAccessor()); 
	        if (value == true){
	        	element.textContent = 'Yes' ;
	        }else if (value == false){
	        	element.textContent = 'No';
	        }
	    }
	};

//Format the currency values
ko.bindingHandlers['mobileCurrency'] = {
	    'update': function (element, valueAccessor) {
	    	 
	    	var value = ko.utils.unwrapObservable(valueAccessor()); 
	    	if (element.nodeName == 'INPUT'){
	    		element.value = value ? "$" + value.toFixed(2) : "";
	    	}else if (element.nodeName == 'SPAN'){
	    		element.textContent = value ? "$" + value.toFixed(2) : "None";
	    	}
	    }
	};

//modal dialog
ko.bindingHandlers['dialogWith'] = {

	    'init': function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
	    	var $element = $(element);
	    	$element.hide();
	    	
	        return ko.bindingHandlers["with"].init(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);
	    },
	    'update': function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
	    //    var t = ko.bindingHandlers["with"].update(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);
	     //   setTimeout(function () {
	        	//$(element).listview('refresh');
	     //       $(element).trigger('pagecreate'); //fixes un-enhanced issues
	     //   }, 0);
	      //  return t;
	    	
	       
	    	//$element.fadeIn();
	    	// setTimeout(function () {
		        	//$(element).listview('refresh');
		  //          $(element).trigger('pagecreate'); //fixes un-enhanced issues
		  //      }, 0);
	    	var t = ko.bindingHandlers["with"].update(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);
	    	setTimeout(function (){
	    		var $element = $(element);
	    		$element.fadeIn();
	    	}, 0);
	    	return t;
	    }

	};


