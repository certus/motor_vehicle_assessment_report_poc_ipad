
/* JavaScript content from js/claimsViewModel.js in folder common */


//wrapper to an observable that requires accept/cancel
ko.protectedobservable = function(initialValue) {
    //private variables
    var _actualValue = ko.observable(initialValue),
        _tempValue = initialValue;

    //computed observable that we will return
    var result = ko.computed({
        //always return the actual value
        read: function() {
           return _actualValue();
        },
        //stored in a temporary spot until commit
        write: function(newValue) {
             _tempValue = newValue;
        }
    });

    //if different, commit temp value
    result.commit = function() {
        if (_tempValue !== _actualValue()) {
             _actualValue(_tempValue);
        }
    };

    //force subscribers to take original
    result.reset = function() {
        _actualValue.valueHasMutated();
        _tempValue = _actualValue();   //reset temp value
    };

    return result;
};


function ClaimsData(data){
	var self = this;
	self.ClaimId = ko.observable(data.ClaimId);
	self.Year = ko.observable(data.Year);
	self.Make = ko.observable(data.Make);
	self.Model = ko.observable(data.Model);
	self.RegistrationNo = ko.observable(data.RegistrationNo);
	self.Suburb = ko.observable(data.Suburb);
	self.Transmission = ko.observable(data.Transmission);
	self.Insured = ko.observable(data.Insured);
	self.ThirdParty = ko.observable(data.ThirdParty);
	self.SumInsured = ko.observable(data.SumInsured);
	self.Excess = ko.observable(data.Excess);
	self.Age = ko.observable(data.Age);
	self.ClaimFormWithVehicle = ko.observable(data.ClaimFormWithVehicle);
	self.RepairerName = ko.observable(data.RepairerName);
	self.RepairerPhone = ko.observable(data.RepairerPhone);
	self.Address = ko.observable(data.Address);
	
	self.ModelDesc = ko.computed (function(){
        return self.Year() + " " + self.Make() + " " + self.Model() + " - " + self.RegistrationNo();
    },this);
	self.ModelDesc_assessed = ko.computed (function(){
		return self.Make() + " " + self.Model() + " - " + self.RegistrationNo();
	},this);
	
	self.DoorsOptions = [2,3,4,5,6,7];
	self.NumberOfDoors =  ko.observable(data.NumberOfDoors);
	self.Odometer = ko.observable(data.Odometer);
	self.Colour = ko.observable(data.Colour);
	self.EngineNo = ko.observable(data.EngineNo);
	self.BodyType = ko.observable(data.BodyType);
	self.ChassisNo = ko.observable(data.ChassisNo);
	self.RegistrationExpiry = ko.observable(data.RegistrationExpiry);
	
}

ClaimsData.prototype.toJSON = function() {
    var copy = ko.toJS(this); //easy way to get a clean copy
    delete copy.Year; 
    delete copy.Make;
    delete copy.Model;
    delete copy.RegistrationNo;
    delete copy.Suburb;
    delete copy.Transmission;
    delete copy.Insured;
    delete copy.ThirdParty;
    delete copy.SumInsured;
    delete copy.Excess;
    delete copy.Age;
    delete copy.ClaimFormWithVehicle;
    delete copy.RepairerName;
    delete copy.RepairerPhone;
    delete copy.Address;
    delete copy.ModelDesc;
    delete copy.ModelDesc_assessed;
    delete copy.DoorsOptions;
    return copy; //return the copy to be serialized
};

function VehicleCondition(data){
	var self = this;
	self.ClaimId = ko.observable(data.ClaimId);
	self.Brakes = ko.observable (data.Brakes);
    self.Steering = ko.observable (data.Steering);
    self.PaintInterior = ko.observable (data.PaintInterior);
    self.PreAccidentValue = ko.observable(data.PreAccidentValue);
    self.ApproxSalvageValue = ko.observable(data.ApproxSalvageValue);
    self.SalvageRefNo = ko.observable(data.SalvageRefNo);
    self.TyreTreadRHF = ko.observable(data.TyreTreadRHF);
    self.TyreTreadRHR = ko.observable(data.TyreTreadRHR);
    self.TyreTreadLHF = ko.observable(data.TyreTreadLHF);
    self.TyreTreadLHR = ko.observable(data.TyreTreadLHR);
    self.AccessoriesModifications = ko.observable(data.AccessoriesModifications);    
	self.TyreTreadOptions = ["Good","Average","Poor"];
}

function Mod (data){
	var self = this;
	self.ModId = ko.observable(data.ModId);
	self.ClaimId = ko.observable(data.ClaimId);
	self.Title = ko.protectedobservable(data.Title);
	self.Description = ko.protectedobservable(data.Description);
	
    self.commitAll = function() {
        this.Title.commit();
        this.Description.commit();
    };
    self.resetAll = function() {
        this.Title.reset();
        this.Description.reset();
    };

};

function Picture (ClaimId, PhotoId, imageData){
	var self = this;
	self.ClaimId = ko.observable(ClaimId);
	self.PhotoId = ko.observable(PhotoId);
	self.Title =  ko.computed (function(){
        return "Image" + self.PhotoId().toString();
    },this);
	self.Src = ko.observable("data:image/jpeg;base64," + imageData);
	self.damages = ko.observableArray([]);
};

function Damage (data){
	var self = this;
	//self.ClaimId = ko.observable(ClaimId);
	self.PhotoId = ko.observable(data.PhotoId);
	self.DamageId = ko.observable(data.DamageId);
	self.Type = ko.observable(data.Type);
	self.Coords = ko.observableArray([]);
	self.DrawColor = ko.observable(data.DrawColor);
}

function DamageType (Type, Color){
	var self = this;
	self.Type = ko.observable (Type);
	self.DrawColor = ko.observable (Color);
	self.BorderStyle = ko.computed (function(){
		return "4px solid " + self.DrawColor();
	},this);
	
}

var ClaimsViewModel = function(){
	
	var self = this;
	self.claims = ko.observableArray([]);
	self.assessed_claims = ko.observableArray([]);
	self.selectedClaim = ko.observable();
	
	//conditions array stores the VehicleConditions object for each claim.
	self.conditions = ko.observableArray([]);
	self.selectedClaim.condition = ko.observable();
	
	//modifications
	self.mods = ko.observableArray([]);
	self.currentMod =  ko.observable();
	self.newMod = ko.observable(true);
	
	//damage modifications
	self.pictures = ko.observableArray([]);
	self.currentPicture = ko.observable();
	self.damageTypes = ko.observableArray([]);
	self.currentDamageType = ko.observable();
	//self.currentPicture.damages = ko.observableArray([]);
	self.currentDamage = ko.observable();
	
	self.loadClaimsView = function(){
		busy=new WL.BusyIndicator("list-page",{text: "Loading..."});
    	busy.show();
    	
		var invocationData = {
				adapter: 'InsuranceAdapter',
				procedure: 'getClaims',
				parameters: []
		};
		WL.Client.invokeProcedure(invocationData,{
				onSuccess : function(result){
					WL.Logger.debug("Successful retrieval of claims list from sql server");
					if (result.invocationResult.resultSet.length > 0){
						var allData = result.invocationResult.resultSet;
						$.map(allData, function(item) {
							if (item.Status === 'Active'){
								self.claims.push(new ClaimsData(item)); 
							}else{ 
								self.assessed_claims.push(new ClaimsData(item));
							}
						});
						busy.hide();
					}
				},
				onFailure: function(){
					WL.Logger.debug("Error retrieving claims list from sql server");
					//do something
					busy.hide();
				}
			}
		);		
	};	

	self.editClaim = function(item){
		self.selectedClaim (item);
		WL.Logger.debug("editing claim");
		$.mobile.changePage("#claim-details");	
	};
	
	self.openCondition = function(data){
		var me = this;
		var conditionFound = false;
		//check if there is a vehiclecondition obj for this claim item  in conditions array-
		ko.utils.arrayForEach(self.conditions(), function(item) {
			WL.Logger.debug ("data is " + data.ClaimId());
			WL.Logger.debug ("item is " + item.ClaimId());
			var dataClaimId = data.ClaimId();
			var itemClaimId = item.ClaimId();
			if (dataClaimId == itemClaimId){
				WL.Logger.debug ("Found the condition data for " + item.ClaimId());
				self.selectedClaim.condition (item);
				conditionFound = true;
				$.mobile.changePage("#condition-details");	
				return false;
			}
	    });
		//condition object is not found in the conditions array so lets load from db
		if (conditionFound==false){
			self.loadCondition(data);
		}
		
	};
		
	self.loadCondition = function (item){
		var invocationData = {
				adapter: 'InsuranceAdapter',
				procedure: 'getVehicleConditionDetails',
				parameters: [item.ClaimId()]
		};
		WL.Client.invokeProcedure(invocationData,{
				onSuccess : function(result){
					WL.Logger.debug("Successful retrieval of condition details from sql server");
					if (result.invocationResult.resultSet.length > 0){
						var allData = result.invocationResult.resultSet;
						$.map(allData, function(item) { 
							//self.selectedClaimData( item);
							var o = new VehicleCondition(item);
							
							self.selectedClaim.condition(o);
						//	console.log(ko.isObservable(self.claims), ko.utils.unwrapObservable(self.claims));
							self.conditions.push(o);
							
							$.mobile.changePage("#condition-details");
						});
					}
				},
				onFailure: function(){
					WL.Logger.debug("Error retrieving condition details from sql server");
					//do something
				}
			}
		);
		
	};
	
	//Save Vehicle Info
	self.saveVehicleDetails = function (item, data) {
		var me = this;
		me.queryString = null;
		var ClaimId = data.ClaimId();
	
		var invocationData = {
				adapter: 'InsuranceAdapter',
				procedure: 'updateVehicleDetails',
				parameters: [item,ClaimId ]
		};
		WL.Client.invokeProcedure(invocationData,{
				onSuccess : function(result){
					WL.Logger.debug ("Successful update of vehicle details into database");
					$.mobile.changePage ("#list-page");
				},
				onFailure: function(){
					WL.Logger.debug("Error updating vehicle info into sql server");
					//do something
				}
			});
	};
	
	//Save Vehicle Condition Info
	self.saveVehicleConditionDetails = function (item, data) {
		
		var ClaimId = data.ClaimId();
	
		var invocationData = {
				adapter: 'InsuranceAdapter',
				procedure: 'updateVehicleConditionDetails',
				parameters: [item, ClaimId ]
		};
		WL.Client.invokeProcedure(invocationData,{
				onSuccess : function(result){
					WL.Logger.debug ("Successful update of vehicle condition details into database");
					$.mobile.changePage ("#list-page");
				},
				onFailure: function(){
					WL.Logger.debug("Error updating vehicle info into sql server");
					//do something
				}
			});
	};
	
	//Modifications ---------------------------------------------------------------
	self.editMod = function (modItem){
		self.newMod(false);
		self.currentMod (modItem);
		$.mobile.changePage("#dialog", {role: 'dialog'});
	};
	
	self.addMod = function (data){
		self.newMod(true);
		var newMod = new Mod ({
			ClaimId: data.selectedClaim().ClaimId(), 
			Title:'', 
			Description:''
		});
		self.currentMod (newMod);
		$.mobile.changePage("#dialog", {role: 'dialog'});
	};
	
	self.removeMod = function (data) {
				 
		var invocationData = {
				adapter: 'InsuranceAdapter',
				procedure: 'deleteVehicleMod',
				parameters: [ data.ModId()]
		};
		WL.Client.invokeProcedure(invocationData,{
				onSuccess : function(result){
					WL.Logger.debug ("Successful delete of mod from database");
					self.mods.remove(data);
					self.currentMod (undefined);
					
				},
				onFailure: function(){
					WL.Logger.debug("Error deleting mod from database");
					//do something
				}
			});
		$("#mods-ul").listview('refresh');
	};
	
	
	self.loadMods = function(data){
		//load mods from db using claim.ClaimId
		//console.log(data.ClaimId());
		self.mods.removeAll();
		var invocationData = {
				adapter: 'InsuranceAdapter',
				procedure: 'getVehicleMods',
				parameters: [data.ClaimId()]
		};
		WL.Client.invokeProcedure(invocationData,{
				onSuccess : function(result){
					WL.Logger.debug("Successful retrieval of mods from sql server");
					if (result.invocationResult.resultSet.length > 0){
							
						var allData = result.invocationResult.resultSet;
						$.map(allData, function(item) { 
							//self.selectedClaimData( item);
							var o = new Mod(item);
								
						//	console.log(ko.isObservable(self.claims), ko.utils.unwrapObservable(self.claims));
							self.mods.push (o);							
							
						});						
					}
				},
				onFailure: function(){
					WL.Logger.debug("Error retrieving mod details from sql server");
					//do something
				}
				
			});
		$.mobile.changePage("#mods-page");
	
		
	};
	
	self.saveMod = function(data) {
	
		self.currentMod().commitAll();
		
		if (self.newMod() == true){
			self.mods.push(self.currentMod());
					
			//insert into table
			var invocationData = {
					adapter: 'InsuranceAdapter',
					procedure: 'insertVehicleMods',
					parameters: [data.ClaimId(), data.Title(), data.Description()]
			};
			WL.Client.invokeProcedure(invocationData,{
					onSuccess : function(result){
						WL.Logger.debug ("Successful insert of mod details into database");
						
					},
					onFailure: function(){
						WL.Logger.debug("Error inserting mod into sql server");
						//do something
					}
				});
		}else {
			var invocationData = {
					adapter: 'InsuranceAdapter',
					procedure: 'updateVehicleMods',
					parameters: [ data.Title(), data.Description(), data.ModId()]
			};
			WL.Client.invokeProcedure(invocationData,{
					onSuccess : function(result){
						WL.Logger.debug ("Successful update of mod details into database");
						
					},
					onFailure: function(){
						WL.Logger.debug("Error updating mod into sql server");
						//do something
					}
				});
		}
        self.currentMod(undefined);
        
        $("#dialog").dialog('close');
        $("#mods-ul").listview('refresh');
        //to do - update to db
    };
   
    self.cancelModEdit = function(){
    	self.currentMod().resetAll();
    	self.currentMod(undefined);
    	$("#dialog").dialog('close');
    	 $("#mods-ul").listview('refresh');
   
    };
   	//-------------------------------------------------------------------------------------------------
    
    //Damage Assessment ------------------------------------------------------------------------------
    
    self.photoOptions = ko.observableArray(['From Camera', 'From Photo Library']);
    
    self.showPhotoOptions = function (data){
    	$.mobile.changePage("#damage-dialog", {role: 'dialog'});
    };
    
       
    self.loadPictures = function(data){
    	
    	//console.log(data.ClaimId());
    	busy=new WL.BusyIndicator("damage-page",{text: "Loading..."});
    	busy.show();
    	
		self.pictures.removeAll();
		var invocationData = {
				adapter: 'InsuranceAdapter',
				procedure: 'getPhotos',
				parameters: [data.ClaimId()]
		};
		WL.Client.invokeProcedure(invocationData,{
				onSuccess : function(result){
					WL.Logger.debug("Successful retrieval of pics from sql server");
					if (result.invocationResult.resultSet.length > 0){
							
						var allData = result.invocationResult.resultSet;
						$.map(allData, function(item) { 
							//self.selectedClaimData( item);
							var o = new Picture(data.ClaimId(),item.PhotoId,item.Image);
								
						//	console.log(ko.isObservable(self.claims), ko.utils.unwrapObservable(self.claims));
							self.pictures.push (o);						
							
						});	
						busy.hide();
					}
				},
				onFailure: function(){
					WL.Logger.debug("Error retrieving photo details from sql server");
					//do something
					busy.hide();
				}
				
			});
		
    	$.mobile.changePage("#damage-page");
    
    };
    
    self.addPicture = function (data, imageData){
    	// insert into table, get the photo id back.
    	
    	var invocationData = {
				adapter: 'InsuranceAdapter',
				procedure: 'insertPhoto',
				parameters: [data.selectedClaim().ClaimId(), imageData]
		};
		WL.Client.invokeProcedure(invocationData,{
				onSuccess : function(result){
					WL.Logger.debug("Photo inserted into sql server");
					
					if (result.invocationResult.resultSet.length > 0){
							
						var allData = result.invocationResult.resultSet;
						$.map(allData, function(item) { 
							//self.selectedClaimData( item);
							var o = new Picture(data.selectedClaim().ClaimId(),item.PhotoId,imageData);
								
						//	console.log(ko.isObservable(self.claims), ko.utils.unwrapObservable(self.claims));
							self.pictures.push (o);							
							self.currentPicture (o);
						});						
					}
				},
				onFailure: function(){
					WL.Logger.debug("Error retrieving photo details from sql server");
					//do something
				}
				
			});
    	
				
		$.mobile.changePage ("#damagemods-page");
		WL.Logger.debug ("Changed page to damagemods-page");
		
    };
    
    self.editPicture = function(item){
    	self.currentPicture ( item );
    	if (self.currentPicture().damages().length === 0){
    		self.loadDamages (item);
    	}else {
    		$.mobile.changePage("#damagemods-page");
    	}
    	
    	
    };
    
    // get picture from camera or photo library.
    self.getPicture = function(data, item) {
   	 	WL.Logger.debug("get picture");
   	 	var optionsVar = { quality: 50, destinationType: navigator.camera.DestinationType.DATA_URL };
   	 	
   	 	if (item == "From Photo Library"){
   	 		optionsVar["sourceType"] = pictureSource.PHOTOLIBRARY;
   	 	}
   	 	navigator.camera.getPicture(
    		function (imageData) {
    			WL.Logger.debug(imageData);
    			
    			// load to db table - 
    			self.addPicture (data, imageData);
    			
    		}, 
    		function (message) {
    			alert('Failed because: ' + message);
    			WL.Logger.debug("photo capture failed " + message);
    		},
    		optionsVar
    		); 	
      
   };
    
   self.removePicture = function (data) {
		 
		var invocationData = {
				adapter: 'InsuranceAdapter',
				procedure: 'deletePhoto',
				parameters: [ data.PhotoId()]
		};
		WL.Client.invokeProcedure(invocationData,{
				onSuccess : function(result){
					WL.Logger.debug ("Successful delete of photo from database");
					self.pictures.remove(data);
					self.currentPicture (undefined);
					
				},
				onFailure: function(){
					WL.Logger.debug("Error deleting photo from database");
					//do something
				}
			});
		$("#damage-ul").listview('refresh');
	};    
	
   self.loadDamageTypes = function(){
	   self.damageTypes.push (new DamageType("New Damage","#FF0000"));
	   self.damageTypes.push (new DamageType("Old Damage","#00FF00"));
	   self.damageTypes.push (new DamageType("Rust","#EE8006"));
	   self.damageTypes.push (new DamageType("Dent","#5B5B5B"));
	   self.damageTypes.push (new DamageType("Scratch","#004080"));
	   
   };
   
   
   self.setDamageType = function(data, item){
	   self.currentDamageType (item);
	   setCanvasLineColor (item.DrawColor());
	   
	   //Locate the damage object for this damage type and set the current damage.
	   //We need this to write the coordinates from the touch events functions.
	//   var d = ko.utils.arrayFirst (self.currentPicture().damages(), function(damage){
	//	   return (damage.Type() == self.currentDamageType().Type()); 			
	//	 });
	 //  if (d){
	//	   self.currentDamage (d);
	//   }else{
		   self.newDamage (data, item) ;
	 //  }
		   
   };
     
	
   self.loadDamages = function (item){	   
	   //check if damages array already has the damage item for this image
	   WL.Logger.debug("loading damages for photo " + item.PhotoId());
	 //  self.currentPicture.damages.removeAll();
	   
	   var invocationData = {
				adapter: 'InsuranceAdapter',
				procedure: 'getDamages',
				parameters: [item.PhotoId()]
		};
		WL.Client.invokeProcedure(invocationData,{
				onSuccess : function(result){
					WL.Logger.debug("successful retrieval of damage coordinates from sql server");
					
					if (result.invocationResult.resultSet.length > 0){
							
						var allData = result.invocationResult.resultSet;
						$.map(allData, function(item) { 
							
							var o = new Damage(item);
							
							$.each(item.Coords, function(index,value){
								var tempVal = {
									CoordinateX : value.CoordinateX,
									CoordinateY : value.CoordinateY								
								};
								o.Coords.push(tempVal);
							});
						
							self.currentPicture().damages.push (o);							
						});						
					}
					$.mobile.changePage("#damagemods-page");
				},
				onFailure: function(){
					WL.Logger.debug("Error retrieving damages from sql server");
					//do something
					$.mobile.changePage("#damagemods-page");
				}
				
			});	   	
	   	
	// self.damages.push (new Damage (1,"New Damage",[{x: 270,y:80}]));
	// self.damages.push (new Damage (2,"Old Damage",[{x: 280,y:90}])); 
	
	 return true;
   };

   self.newDamage = function (data, item){
	   var o = new Damage ({
		  Type: item.Type(),
		  DrawColor: item.DrawColor(),
		  PhotoId : data.currentPicture().PhotoId()
	   });
	//   o.PhotoId (data.currentPicture().PhotoId());
	   self.currentPicture().damages.push (o);
	   self.currentDamage (o);
   };
   
   self.saveDamageModifications = function(data, item){
	   //data - root or self object
	   //item - currentPicture
	   var alldamages = data.currentPicture().damages();
	   for (var k=0;k<alldamages.length;k++){
			var d = alldamages[k];
			
			if (!d.DamageId()){
				//if damageId is null then we need to insert into both damages and coordinates table
				 var invocationData = {
							adapter: 'InsuranceAdapter',
							procedure: 'insertDamage',
							parameters: [ko.toJS(d), item.PhotoId()]
					};
					WL.Client.invokeProcedure(invocationData,{
							onSuccess : function(result){
								WL.Logger.debug("successful insert of damages into sql server");						
								 
							},
							onFailure: function(){
								WL.Logger.debug("Error inserting damages into sql server");
								//do something
								//  $.mobile.changePage ("#damagemods-page");
							}
							
						});	   	
			}else {
				 //if damageId is not null then we update rows in the coordinates table
				/*
				 var invocationData = {
							adapter: 'InsuranceAdapter',
							procedure: 'updateDamageCoordinates',
							parameters: [ko.toJS(d), d.DamageId()]
					};
					WL.Client.invokeProcedure(invocationData,{
							onSuccess : function(result){
								WL.Logger.debug("successful update of damages into sql server");						
								return true;
							},
							onFailure: function(){
								WL.Logger.debug("Error inserting damages into sql server");
								//do something
							}
							
						});	  
					*/	
			}
		}
	   return true;   
	   
   };
   
   
	//Initial load of claims view
	self.loadClaimsView();
	self.loadDamageTypes();
};



