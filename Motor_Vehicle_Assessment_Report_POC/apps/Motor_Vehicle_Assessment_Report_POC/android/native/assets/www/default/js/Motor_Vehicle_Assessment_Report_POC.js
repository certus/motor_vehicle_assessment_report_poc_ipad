
/* JavaScript content from js/Motor_Vehicle_Assessment_Report_POC.js in folder common */
window.$ = window.jQuery = WLJQ;

var pictureSource;   // picture source
var destinationType; // sets the format of returned value

var busy;
var canvas;
var context;
var claimsVM;
$(document).bind("mobileinit", function(){
    $.mobile.defaultPageTransition = 'none';
    $.mobile.defaultDialogTransition = 'none';
    $.mobile.useFastClick = true;
    
});


function  wlCommonInit(){
	WL.Logger.debug("in wlCommonInit");
	busy=new WL.BusyIndicator('list-page');
	//loadNewClaims();
	//ko.virtualElements.allowedBindings.mobileWith = true;
	claimsVM = new ClaimsViewModel();
	ko.applyBindings(claimsVM);
	//$(".numeric").numeric();
	if (navigator.camera){
		pictureSource=navigator.camera.PictureSourceType;
		destinationType=navigator.camera.DestinationType;
	}
	
}



$( document ).delegate("#damagemods-page", "pageshow", function() {
	canvas = document.getElementById("damage_canvas");
	canvas.addEventListener('touchstart', draw, false);
	canvas.addEventListener('touchmove',draw, false);
	canvas.addEventListener('touchend', draw, false);
	// prevent elastic scrolling
	canvas.addEventListener('touchmove', function (event) {
       event.preventDefault();
    }, false); 
	
	drawCanvas();
});



function drawCanvas() {
	WL.Logger.debug("draw canvas");
	context = canvas.getContext("2d");
	//context.canvas.width  = window.innerWidth;
	//context.canvas.height = window.innerHeight;
	
	var img_hidden = $('#smallImage');
	
	var img = new Image();
	img.src="";
	$(img).one("load",function(){
		
		
		var img = this;
		context.drawImage(img, 10, 10,700,500);	
		WL.Logger.debug("image drawn");
		
		//get the coords
		var alldamages = claimsVM.currentPicture().damages();
		
		for (var k=0;k<alldamages.length;k++){
			var d = alldamages[k];
			//set the color
			context.beginPath();
			context.strokeStyle = d.DrawColor();
			
			//draw the lines
			var l = d.Coords().length;
		
			for (var i=0;i<l;i++){
				var coors = d.Coords()[i];
				//context.strokeStyle = d.DrawColor();
				
				if (i===0){
					context.moveTo(coors.CoordinateX, coors.CoordinateY);
				}else {
					context.lineTo(coors.CoordinateX, coors.CoordinateY);
		            context.stroke();
				}
			}
		}
		
	})
	.each(function(){
		if(this.complete) $(this).trigger("load");
	});


	img.src = img_hidden.attr("src");
	
		
	
}

//create a function to pass touch events and coordinates to drawer
function draw(event) {
	 
    // get the touch coordinates.  Using the first touch in case of multi-touch
    var coors = {
       CoordinateX: event.targetTouches[0].pageX,
       CoordinateY: event.targetTouches[0].pageY
    };

    // Now we need to get the offset of the canvas location
    var obj = canvas;

    if (obj.offsetParent) {
       // Every time we find a new object, we add its offsetLeft and offsetTop to curleft and curtop.
       do {
          coors.CoordinateX -= obj.offsetLeft;
          coors.CoordinateY -= obj.offsetTop;
       } while ((obj = obj.offsetParent) != null);
    }

    // pass the coordinates to the appropriate handler
    touchEventHandler[event.type](coors);
 }

//Drawer object which has the event handler functions
var touchEventHandler = {
        isDrawing: false,
        touchstart: function (coors) {
        	WL.Logger.debug("touch start");
        	WL.Logger.debug("beg x coords =" + coors.CoordinateX);
        	WL.Logger.debug("beg y coords =" + coors.CoordinateY);
           context.beginPath();
           context.moveTo(coors.CoordinateX, coors.CoordinateY);
           claimsVM.currentDamage().Coords.push(coors);
           this.isDrawing = true;
        },
        touchmove: function (coors) {
           if (this.isDrawing) {
              context.lineTo(coors.CoordinateX, coors.CoordinateY);
              context.stroke();
              claimsVM.currentDamage().Coords.push(coors);
             // WL.Logger.debug("move x coords =" + coors.x);
          	//  WL.Logger.debug("move y coords =" + coors.y);
           }
        },
        touchend: function (coors) {
           if (this.isDrawing) {
        	   WL.Logger.debug("end x coords =" + coors.CoordinateX);
           		WL.Logger.debug("end y coords =" + coors.CoordinateY);
           	  this.touchmove(coors);
             
              this.isDrawing = false;
           }
        }
};


function loadCanvasImage(event){
	
}

function setCanvasLineColor (color){
	context.strokeStyle = color;
}



/*
 * 
 * var modsList = {
		title: '',
		desc : ''
};
//$(document).bind( "pagechange", function( e, data ) {
		
//	if ($(data.toPage)){
//	if ( data.toPage [0].id == "mods-page" ) {
//		loadMods();
//	}}});

//Call native camera function
function takePicture() {
	 WL.Logger.debug("take picture");
   navigator.camera.getPicture(onPhotoSuccess, onPhotoFail, { quality: 50,
     destinationType: navigator.camera.DestinationType.DATA_URL,sourceType : navigator.camera.PictureSourceType.CAMERA });
   
}

function onPhotoSuccess (imageData) {
	$.mobile.changePage ("#damagemods-page");
	WL.Logger.debug ("Changed page to damagemods-page");
    var smallImage = $('#smallImage');

    smallImage.style.display = 'block';

    smallImage.src = "data:image/jpeg;base64," + imageData;
    $("#image-div").trigger('create');
 }

function onPhotoFail(message) {
   alert('Failed because: ' + message);
   WL.Logger.debug("photo capture failed " + message);
 }

function recordVoice (){
	navigator.device.capture.captureAudio(captureSuccess, captureError, {limit: 2});
}

function captureSuccess(mediaFiles){
	  navigator.notification.alert("Audio captured");
}

function captureError (error){
	var msg = 'An error occurred during capture: ' + error.code;
    navigator.notification.alert(msg, null, 'Error!');

}

$('#dialog_header').click = function(e){
	$( "#myPopupDiv" ).popup( "close" );
};



function addMod(){
		
	modsList = {
			title: $('#title').val(),
			desc:  $('#desc').val()
	};
		
	//$ul.listview('refresh'); 
	$('#title').val("");
	$('#desc').val("");
	//$.mobile.changePage('mods.html');
	$('.ui-dialog').dialog('close') ;
	
}	


function loadMods(){
	if (modsList['title'] != ""){
		
	html1 = '<li data-role="list-divider" role="heading">';
	html2 = '</li><li  data-corners="false" data-shadow="false" data-iconshadow="true" data-wrapperels="div" data-icon="arrow-r" data-iconpos="right" data-theme="d">';	
	html = html1 + modsList['title'] + html2 + modsList['desc'] + "</li>";
	//html = html1 + modsList['title'] + "<\p><\li>";
	var $ul = $('#mods-ul');
	
	$ul.append(html);
	if ($ul.hasClass('ui-listview')) {
		$ul.listview('refresh');
    } else {
    	$ul.trigger('create');
    }
	modsList = {
			title: '',
			desc:  ''
	};
	}
} 
*/


/* JavaScript content from js/Motor_Vehicle_Assessment_Report_POC.js in folder android */

// This method is invoked after loading the main HTML and successful initialization of the Worklight runtime.
function wlEnvInit(){
    wlCommonInit();
    // Environment initialization code goes here
}