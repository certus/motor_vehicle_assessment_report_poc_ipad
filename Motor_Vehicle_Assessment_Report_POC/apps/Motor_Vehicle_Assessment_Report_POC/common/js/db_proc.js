function loadClaims(param){
	WL.Logger.debug("Loading claims..");
	var invocationData = {
			adapter: 'InsuranceAdapter',
			procedure: 'getClaims',
			parameters: [param]
	};
	WL.Client.invokeProcedure(invocationData,{
		onSuccess : loadClaimsSuccess,
		onFailure: loadClaimsFailure
		}
	);
	
}
/*
function loadClaims(param){
	WL.Logger.debug("Loading claims..");
	var invocationData = {
			adapter: 'InsuranceAdapter',
			procedure: 'getClaims',
			parameters: [param]
	};
	WL.Client.invokeProcedure(invocationData,{
		onSuccess : function(result){
			WL.Logger.debug("Successful retrieval of claims list from sql server");
			if (result.invocationResult.resultSet.length > 0){
				return result.invocationResult.resultSet;
			}
		},
		onFailure: function(){
			WL.Logger.debug("Error retrieving claims list from sql server");
		}
	}
	);
	
}
*/
function loadClaimsSuccess(result){
	WL.Logger.debug("Successful retrieval of claims list from sql server");
	if (result.invocationResult.resultSet.length > 0)
		displayClaims(result.invocationResult.resultSet);
	}

function loadClaimsFailure(){
	busy.hide();
	WL.Logger.debug("Error getting claims list from sql server");
	claimsData = {
			error: 'Error loading data...'
	};
	//$('#view-list-ul').html("Error loading data...");
}

function loadAssessedClaimsSuccess(result){
	WL.Logger.debug("Successful retrieval of assessed claims list from sql server");
	if (result.invocationResult.resultSet.length > 0)
			displayAssessedClaims(result.invocationResult.resultSet);
}

function displayClaims(items){
	busy.hide();
	var i;
	var $viewul = $('#view-list-ul');
	var htmlText="";
	
	for (i=0;i < items.length;i += 1){
		var modelText = items[i].Year + " " + items[i].Make + " " + items[i].Model + " - " + items[i].RegistrationNo;
		htmlText = htmlText + '<li class="view-row"><a href="claimDetails.html"><div class="ui-grid-b">';
		htmlText = htmlText + '<div class="ui-block-a" >' + modelText + '</div>';
		htmlText = htmlText + '<div class="ui-block-b" >' + items[i].Suburb + '</div>';
		htmlText = htmlText + '<div class="ui-block-c" >0 kms</div>';
		htmlText = htmlText + '<div class="ui-block-d"><img src="images/map_icon.png" /></div>';
		htmlText = htmlText + "</div></a></li>";			
	}
	$viewul.append(htmlText);
	if ($viewul.hasClass('ui-listview')) {
		$viewul.listview('refresh');
    } else {
    	$viewul.trigger('create');
    }
}

function displayAssessedClaims(items){
	busy.hide();
	var i;
	var $viewul = $('#assessed-view-list-ul');
	var htmlText="";
	
	for (i=0;i < items.length;i += 1){
	
		var modelText = items[i].Make + " " + items[i].Model + " - " + items[i].RegistrationNo;
		htmlText = htmlText + '<li class="view-row"><a href="claimDetails.html"><div class="ui-grid-b">';
		htmlText = htmlText + '<div class="ui-block-a" >' + items[i].Year + '</div>';
		htmlText = htmlText + '<div class="ui-block-b" >' + modelText + '</div>';
		htmlText = htmlText + "</div></a></li>";			
	}
	$viewul.append(htmlText);
	if ($viewul.hasClass('ui-listview')) {
		$viewul.listview('refresh');
    } else {
    	$viewul.trigger('create');
    }
}