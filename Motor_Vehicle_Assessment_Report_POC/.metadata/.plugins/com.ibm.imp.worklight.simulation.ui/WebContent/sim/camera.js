require(["dojo/dom", "dijit/registry"], function(dom, registry){

	addService("Camera", function(){
		var cameraDir = "";
		var curCameraImage = "";
		var curAlbumImage = "";

		// Public
		// Set image to return for camera
		this.setCameraImage = function(image){
			curCameraImage = image;
			dom.byId("cameraSelectedId").src = cameraDir + curCameraImage;
			dom.byId("cameraSizeId").innerHTML = image;
		};

		// Public
		// Set image to return for library
		this.setAlbumImage = function(image){
			curAlbumImage = image;
			dom.byId("albumSelectedId").src = cameraDir + curAlbumImage;
			dom.byId("albumSizeId").innerHTML = image;
		};

		// Get image for camera or library
		var getCameraImage = function(source){
			_consoleLog("camera image=" + curCameraImage);
			if (source == 1) {
				return cameraDir + curCameraImage;
			}
			return cameraDir + curAlbumImage;
		};

		// Public
		// Handle requests
		this.exec = function(action, args, callbackId){
			// [quality, destinationType, sourceType, targetWidth, targetHeight,
			// encodingType]
			if (action == 'takePicture') {
				var r = getCameraImage(args[2]);
				return new PluginResult(callbackId, PluginResultStatus.OK, r, false);
			}
			return new PluginResult(callbackId, PluginResultStatus.INVALID_ACTION);
		};

		// Initialization
		{
			var n = _pg_sim_nls;

			var td;
			td = dom.byId('sim_camera_choose_image_camera');
			td.innerHTML = n.sim_camera_choose_image_camera;
			td = dom.byId('sim_camera_currently_selected_camera');
			td.innerHTML = n.sim_camera_currently_selected_camera;
			td = dom.byId('sim_camera_choose_image_album');
			td.innerHTML = n.sim_camera_choose_image_album;
			td = dom.byId('sim_camera_currently_selected_album');
			td.innerHTML = n.sim_camera_currently_selected_album;

			sim_camera_imagexs_button.set("label", n.sim_camera_xs);
			sim_camera_images_button.set("label", n.sim_camera_s);
			sim_camera_imagem_button.set("label", n.sim_camera_m);
			sim_camera_imagel_button.set("label", n.sim_camera_l);
			sim_camera_imagexl_button.set("label", n.sim_camera_xl);
			
			sim_camera2_imagexs_button.set("label", n.sim_camera_xs);
			sim_camera2_images_button.set("label", n.sim_camera_s);
			sim_camera2_imagem_button.set("label", n.sim_camera_m);
			sim_camera2_imagel_button.set("label", n.sim_camera_l);
			sim_camera2_imagexl_button.set("label", n.sim_camera_xl);

			sim_camera_albumxs_button.set("label", n.sim_camera_xs);
			sim_camera_albums_button.set("label", n.sim_camera_s);
			sim_camera_albumm_button.set("label", n.sim_camera_m);
			sim_camera_albuml_button.set("label", n.sim_camera_l);
			sim_camera_albumxl_button.set("label", n.sim_camera_xl);
			
			sim_camera2_albumxs_button.set("label", n.sim_camera_xs);
			sim_camera2_albums_button.set("label", n.sim_camera_s);
			sim_camera2_albumm_button.set("label", n.sim_camera_m);
			sim_camera2_albuml_button.set("label", n.sim_camera_l);
			sim_camera2_albumxl_button.set("label", n.sim_camera_xl);
			// Determine base URL for this JS file
			cameraDir = getScriptBase("camera.js") + "camera/";

			this.setCameraImage("camera1_m.jpg");
			this.setAlbumImage("album1_m.jpg");
		}
	});
});