require(["dojo/dom", "dijit/registry"], function(dom, registry){
	addService("Battery", function(){

		_consoleLog("add battery service");
		var currentLevel = 90;
		var isPlugged = false;
		var callback = "";

		// Get battery info
		var getBatteryInfo = function(){
			var r = {
			    isPlugged : isPlugged,
			    level : currentLevel
			};
			return r;
		};

		// Public
		// Called by UI to send change in battery pluggedIn to device
		this.onChange = function(value){
			_consoleLog("*** ONCHANGE=" + value + " ' " + callback + " ' " + (callback ? "true" : "false"));
			isPlugged = value;
			if (callback) {
				sendResult(new PluginResult(callback, PluginResultStatus.OK, getBatteryInfo(), true));
			}
			localStorage.batteryIsPlugged = value;
		};

		// Public
		// Handle requests
		this.exec = function(action, args, callbackId){
			_consoleLog('Battery exec ' + action + " " + args + " " + callbackId);
			if (action == 'start') {
				callback = callbackId;
				var r = getBatteryInfo();
				return new PluginResult(callbackId, PluginResultStatus.OK, r, true); // keep
				// callback
			} else if (action == 'stop') {
				var r = getBatteryInfo();
				var cb = callbackId;
				callbackId = "";
				return new PluginResult(cb, PluginResultStatus.OK, r, false);
			}
			return new PluginResult(callbackId, PluginResultStatus.INVALID_ACTION);
		};

		// Initialization
		{
			var n = _pg_sim_nls;
			var v = dom.byId("sim_battery_plugedIn_label");
			v.innerHTML = n.sim_battery_plugedIn_label;

			v = dom.byId("sim_battery_batteryLevel_label");
			v.innerHTML = n.sim_battery_batteryLevel_label;

			// Retrieve settings from local storage
			isPlugged = localStorage.batteryIsPlugged === "true";
			currentLevel = localStorage.batteryLevel;
			if (!currentLevel) {
				currentLevel = 90;
			}
			if (isPlugged) {
				dom.byId("sim_battery_pluggedIn").checked = isPlugged;
			}

			// Set initial values
			sim_battery_hslider.onChange = function(value){
				currentLevel = value;
				dom.byId("sim_battery_batteryLevel").innerHTML = currentLevel + "%";
				if (callback)
					sendResult(new PluginResult(callback, PluginResultStatus.OK, getBatteryInfo(), true));
				localStorage.batteryLevel = currentLevel;
			};
			sim_battery_hslider.set('value', currentLevel);
			getBatteryInfo();
		}

	});
});