require([
        "dojo/dom",
        "dojo/_base/connect",
        "dojo/_base/html",
        "dojo/has",
        "dojo/dom-construct",
        "dojo/dom-geometry",
        "dojo/_base/lang",
        "dojo/dom-style",
        "dijit/registry",
        "dojox/geo/openlayers/WidgetFeature"], function(dom, connect, html, has, domConstruct, domGeom, lang, domStyle, registry, WidgetFeature){
	addService("Geolocation", function(){

		var runningId = null;
		this.widgetFeature = null;
		this.compass = null;

		this.latitude = randRange(180, 2);
		this.longitude = randRange(360, 2);
		this.accuracy = rand(100, 1);
		this.altitude = rand(10000, 1);
		this.heading = rand(360, 0);
		this.speed = rand(100, 2);
		this.altitudeAccuracy = 1;
		this.stepToNextLL = 0.2;

		// Get current geolocation values
		this.getGeolocation = function(){
			var r = {};
			r.latitude = this.latitude;
			r.longitude = this.longitude;
			r.accuracy = this.accuracy;
			r.altitude = this.altitude;
			r.heading = this.heading;
			r.speed = this.speed;
			r.altitudeAccuracy = this.altitudeAccuracy;
			var d = new Date();
			var t = d.getTime();
			return {
			    coords : r,
			    timestamp : t
			};
		};

		this.updateMap = function(){
			var map = registry.byId('geolocationMap');
			if (map) {
				map = map.map;
				var from = dojox.geo.openlayers.EPSG4326;
				var to = map.olMap.getProjectionObject();
				var lon = parseFloat(this.longitude);
				var lat = parseFloat(this.latitude);
				var p = {
				    x : lon,
				    y : lat
				};
				OpenLayers.Projection.transform(p, from, to);
				map.olMap.setCenter(new OpenLayers.LonLat(p.x, p.y));

			}
		};

		this.updateUI = function(map){
			dom.byId("geoLat").value = this.latitude;
			dom.byId("geoLng").value = this.longitude;
			dom.byId("geoAcc").value = this.accuracy;
			dom.byId("geoAlt").value = this.altitude;
			dom.byId("geoAltAcc").value = this.altitudeAccuracy;
			dom.byId("geoHead").value = this.heading;
			dom.byId("geoVel").value = this.speed;
			if (map)
				this.updateMap();
		};

		this.nextLoc = function(){
			var ret;
			if (rand(10) > 9.5) {
				ret = {
				    latitude : randRange(180, 2),
				    longitude : randRange(360, 2),
				    altitude : rand(10000, 1)
				};
			} else {
				ret = {
				    latitude : this.nextLatitude(),
				    longitude : this.nextLongitude(),
				    altitude : this.nextAltitude()
				};
			}
			return ret;
		};

		this.nextAltitude = function(){
			var a = parseFloat(this.altitude);
			a += randRange(this.stepToNextLL, 2);
			if (a > 10000)
				a = a - 10000;
			if (a < -0)
				a += a;
			return a.toFixed(2);
		};

		this.nextLatitude = function(){
			var l = parseFloat(this.latitude);
			l += randRange(this.stepToNextLL, 2);
			if (l > 90)
				l = l - 90;
			if (l < -90)
				l += 90;
			return l.toFixed(2);
		};

		this.nextLongitude = function(){
			var l = parseFloat(this.longitude);
			l += randRange(this.stepToNextLL, 2);
			if (l > 180)
				l = l - 180;
			if (l < -180)
				l += 180;
			return l.toFixed(2);
		};
		
		this.nextHeading = function(){
			if (!this.heading || rand(10) > 9)
				this.heading = rand(360);
			else {
				var r = randRange(20);
				this.heading += r;
			}
			if (this.heading <= 0)
				this.heading += 360;
			if (this.heading > 360)
				this.heading -= 360;

			if (this.compass)
				this.compass.set("value", this.heading);
		};

		// Public
		// Generate next geolocation values
		this.nextGeolocation = function(){
			if (has("ff")) {
				var n = _pg_sim_nls;
				var parentNode = dom.byId("geoloc");
				domConstruct.empty(parentNode);
				parentNode.innerHTML = n.sim_geoloc_usingFFgeolocation;
			} else {
				var ll = this.nextLoc();
				this.latitude = ll.latitude;
				this.longitude = ll.longitude;
				this.accuracy = rand(100, 1);
				this.altitude = ll.altitude;
				this.altitudeAccuracy = 1;
//				this.heading = rand(360, 0);
				this.nextHeading();
				this.speed = rand(100, 2);
				this.updateUI(true);
			}
		};

		// Public
		// Update geolocation and send to device
		this.updateGeolocation = function(){
			this.nextGeolocation();
			var loc = this.getGeolocation();

			// Send update to device if geolocation watch is running
			if (runningId !== null) {
				var r = new PluginResult(runningId, PluginResultStatus.OK, loc, true);
				sendResult(r);
			}
		};

		var _sim_geoloc_timer_is_on = false;

		this.timedGeolocUpdate = function(){
			if (_sim_geoloc_timer_is_on == true) {
				this.updateGeolocation();
				setTimeout(lang.hitch(this, this.timedGeolocUpdate), 1000);
			}
		};

		this.startStopTimer = function(){
			if (_sim_geoloc_timer_is_on == false) {
				_sim_geoloc_timer_is_on = true;
				this.timedGeolocUpdate();
				sim_geoloc_startStop_button.set("label", n.sim_common_stop);
			} else {
				_sim_geoloc_timer_is_on = false;
				sim_geoloc_startStop_button.set("label", n.sim_common_start);
			}
		};

		// Public
		// Handle requests
		this.exec = function(action, args, callbackId){
			var r = this.getGeolocation();
			if (action == 'getCurrentLocation') {
				return new PluginResult(callbackId, PluginResultStatus.OK, r, false);
			} else if (action == 'start') {
				runningId = callbackId;
				return new PluginResult(callbackId, PluginResultStatus.OK, r, true); // keep
				// callbackId
			} else if (action == 'stop') {
				runningId = null;
				return new PluginResult(callbackId, PluginResultStatus.OK, r, false);
			}
			return new PluginResult(callbackId, PluginResultStatus.INVALID_ACTION);
		};

		this.createWidget = function(){

			that.compass = compass;
			return div;
		};

		// Initialization
		{
			var n = _pg_sim_nls;
			dom.byId('sim_geoloc_latitude_label').innerHTML = n.sim_geoloc_latitude_label;
			dom.byId('sim_geoloc_longitude_label').innerHTML = n.sim_geoloc_longitude_label;
			dom.byId('sim_geoloc_accuracy_label').innerHTML = n.sim_geoloc_accuracy_label;
			dom.byId('sim_geoloc_altitude_label').innerHTML = n.sim_geoloc_altitude_label;
			dom.byId('sim_geoloc_altitudeAccuracy_label').innerHTML = n.sim_geoloc_altitudeAccuracy_label;
			dom.byId('sim_geoloc_heading_label').innerHTML = n.sim_geoloc_heading_label;
			dom.byId('sim_geoloc_velocity_label').innerHTML = n.sim_geoloc_velocity_label;
			sim_geoloc_next_button.set("label", n.sim_geoloc_next_button);
			sim_geoloc_startStop_button.set("label", n.sim_common_start);

			if (window.OpenLayers != undefined) {
				var longitude = 7.154126;
				var latitude = 43.651748;
				var map = new dojox.geo.openlayers.widget.Map({
				    initialLocation : {
				        position : [longitude, latitude],
				        extent : 2
				    },
				    id : "geolocationMap"
				}, "geolocationMapDiv");
				domStyle.set(map.domNode, {
				    width : 100 + "%",
				    height : 200 + "px"
				});
				map.startup();
				var control = new OpenLayers.Control({
					autoActivate : true
				});
				var that = this;
				OpenLayers.Util.extend(control, {
				    chartSize : 70,
				    draw : function(px){
					    OpenLayers.Control.prototype.draw.apply(this, arguments);
					    if (!this._element) {
						    var b = domGeom.position(map.domNode);
						    var div = dojo.create("div", {
						        unselectable : "on",
						        style : {
							        position : "absolute"
						        }
						    }, this.div);
						    that.compass = new widgets.Compass({
						        background : [10, 20, 200, 0],
						        color : [0x20, 0x20, 0x20],
						        title : 'Heading',
						        width : this.chartSize,
						        height : this.chartSize,
						        min : 0,
						        max : 360,
						        majorTicksInterval : 30,
						        startAngle : 0,
						        endAngle : 360,
						        value : 0,
						        textIndicatorFont : 'normal normal normal 7pt calibri,Helvetica,Arial,sans-serif',
						        font : 'normal normal normal 4pt calibri,Helvetica,Arial,sans-serif',
						        id : "compassWidget"
						    }, div);
						    that.compass.startup();
						    this._element = div;
						    this.map.events.register('moveend', this, this.update);
						    return div;
					    }
				    },
				    update : function(){
					    var b = domGeom.position(map.domNode);
					    var div = this._element;
					    dojo.style(div, {
					        left : b.w / 2 - this.chartSize / 2 + "px",
					        top : b.h / 2 - this.chartSize / 2 + "px",
					    });
				    }
				});

				map.map.olMap.addControl(control);
				control.activate(true);
				// control.moveTo(10, 10);
				connect.connect(map.map.olMap, "setCenter", this, function(center, zoom, dragging, forceZoomChange){
					if (dragging) {
						return;
					}
					var from = map.map.olMap.getProjectionObject();
					var to = dojox.geo.openlayers.EPSG4326;
					var p = {
					    x : center.lon,
					    y : center.lat
					};
					OpenLayers.Projection.transform(p, from, to);
					this.longitude = p.x.toFixed(2);
					this.latitude = p.y.toFixed(2);
					this.updateUI(false);
				});

				this.nextGeolocation();

			}
		}
	});
});