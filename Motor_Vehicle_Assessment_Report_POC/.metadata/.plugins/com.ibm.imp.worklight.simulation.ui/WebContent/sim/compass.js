require(["dojo/dom", "dijit/registry"], function(dom, registry){
	addService("Compass", function(){
		var currentHeading = rand(360);
		var _sim_compass_timer_is_on = false;
		var connectId = null;

		var CompassHeading = function(mh, th, ha, ts){
			this.magneticHeading = mh;
			this.trueHeading = th;
			this.headingAccuracy = ha;
			this.timestamp = ts;
		};

		var CompassError = function(e){
			this.code = e;
		};

		CompassError.COMPASS_INTERNAL_ERR = 0;
		CompassError.COMPASS_NOT_SUPPORTED = 1;

		// Get current heading value
		var getHeading = function(){
			return currentHeading.toFixed(3);
		};

		var getTimeStamp = function(){
			return new Date().getTime();
		};

		var getAccuracy = function(){
			return rand(10) / 10;
		};

		// Public
		// Generate next heading value
		this.nextHeading = function(){
			if (rand(10) > 9)
				currentHeading = rand(360);
			else {
				var r = randRange(20);
				currentHeading += r;
			}
			if (currentHeading <= 0)
				currentHeading += 360;
			if (currentHeading > 360)
				currentHeading -= 360;
			sim_compass_heading.set("value", currentHeading.toFixed(3));
			sim_compass_widget.set("value", currentHeading.toFixed(3));
		};

		this.timedCompassUpdate = function(){
			if (_sim_compass_timer_is_on == true) {
				this.nextHeading();
				setTimeout(dojo.hitch(this, this.timedCompassUpdate), 1000);
			}
		};

		this.isTimerStarted = function(){
			return _sim_compass_timer_is_on;
		};

		this.startStopTimer = function(id){
			if (_sim_compass_timer_is_on == false) {
				_sim_compass_timer_is_on = true;
				this.timedCompassUpdate();
				sim_compass_startStop_button.set("label", n.sim_common_stop);
			} else {
				_sim_compass_timer_is_on = false;
				sim_compass_startStop_button.set("label", n.sim_common_start);
			}
			var cw = dijit.byId("compassWidget");
			cw.set("noChange", _sim_compass_timer_is_on);
			if (_sim_compass_timer_is_on) {
				if (connectId)
					dojo.disconnect(connectId);
			} else
				connectId = dojo.connect(cw, "onValueChanged", null, function(){
					var h = dijit.byId("compassHeading");
					currentHeading = parseFloat(cw.get("value"));
					h.set("value", currentHeading.toFixed(3));
				});
		};

		// Public
		// Handle requests
		this.exec = function(action, args, callbackId){
			switch (action) {
				case 'getCurrentHeading' :
					var mh = getHeading();
					var th = getHeading();
					var ha = getAccuracy();
					var ts = getTimeStamp();

					var r = new CompassHeading(mh, th, ha, ts);
					return new PluginResult(callbackId, PluginResultStatus.OK, r, false);
					break;
				default :
					return new PluginResult(callbackId, PluginResultStatus.INVALID_ACTION);
					break;
			}
		};

		// Initialization
		{
			var n = _pg_sim_nls;
			var l;
			l = dom.byId("sim_compass_heading_label");
			l.innerHTML = n.sim_compass_heading_label;
			
			sim_compass_next_button.set("label", n.sim_compass_next_button);
			sim_compass_startStop_button.set("label", n.sim_common_start);
			
			this.nextHeading();
			var cw = sim_compass_widget;
			connectId = dojo.connect(cw, "onValueChanged", null, function(){
				currentHeading = parseFloat(cw.get("value"));
				sim_compass_heading.set("value", currentHeading.toFixed(3));
			});
		}

	});
});