addService("FileTransfer", function(){
	// Public
	// Handle requests
	this.exec = function(action, args, callbackId){
		try {
			var r = "" + document.pgFileTransferApplet.exec(action, JSON.stringify(args), callbackId); 
			_consoleLog("FILETRANSFER.JS RETURNING FROM JAVA=" + r);
			return r; // Return a string, since it is already JSON encoded
		} catch (e) {
			_consoleLog("ERROR: " + e);
		}
	};

	// Initialization
	{
	}
});
