/*
 * Cordova is available under *either* the terms of the modified BSD license *or* the
 * MIT License (2008). See http://opensource.org/licenses/alphabetical for full text.
 *
 * Copyright (c) 2005-2010, Nitobi Software Inc.
 * Copyright (c) 2010-2011, IBM Corporation
 */

if (!Cordova.hasResource("compass")) {

	Cordova.addResource("compass");
	(function(){

		/**
		 * This class provides access to device Compass data.
		 * 
		 * @constructor
		 */
		var Compass = function(){
			/**
			 * The last known Compass position.
			 */
			this.lastHeading = null;

			/**
			 * List of compass watch timers
			 */
			this.timers = {};
		};

		Compass.ERROR_MSG = ["Not running", "Starting", "", "Failed to start"];

		/**
		 * Asynchronously aquires the current heading.
		 * 
		 * @param {Function}
		 *            successCallback The function to call when the heading data
		 *            is available
		 * @param {Function}
		 *            errorCallback The function to call when there is an error
		 *            getting the heading data. (OPTIONAL)
		 * @param {PositionOptions}
		 *            options The options for getting the heading data such as
		 *            timeout. (OPTIONAL)
		 */
		Compass.prototype.getCurrentHeading = function(successCallback, errorCallback, options){

			// successCallback required
			if (typeof successCallback !== "function") {
				_consoleLog("Compass Error: successCallback is not a function");
				return;
			}

			// errorCallback optional
			if (errorCallback && (typeof errorCallback !== "function")) {
				_consoleLog("Compass Error: errorCallback is not a function");
				return;
			}

			// Get heading
			Cordova.exec(successCallback, errorCallback, "Compass", "getCurrentHeading", options);
		};

		/**
		 * Asynchronously aquires the heading repeatedly at a given interval.
		 * 
		 * @param {Function}
		 *            successCallback The function to call each time the heading
		 *            data is available
		 * @param {Function}
		 *            errorCallback The function to call when there is an error
		 *            getting the heading data. (OPTIONAL)
		 * @param {HeadingOptions}
		 *            options The options for getting the heading data such as
		 *            timeout and the frequency of the watch. (OPTIONAL)
		 * @return String The watch id that must be passed to #clearWatch to
		 *         stop watching.
		 */
		Compass.prototype.watchHeading = function(successCallback, errorCallback, options){

			// Default interval (100 msec)
			var frequency = (options !== undefined) ? options.frequency : 100;

			// successCallback required
			if (typeof successCallback !== "function") {
				_consoleLog("Compass Error: successCallback is not a function");
				return;
			}

			// errorCallback optional
			if (errorCallback && (typeof errorCallback !== "function")) {
				_consoleLog("Compass Error: errorCallback is not a function");
				return;
			}

			// Make sure compass timeout > frequency + 10 sec
			Cordova.exec(function(timeout){
				if (timeout < (frequency + 10000)) {
					Cordova.exec(null, null, "Compass", "setTimeout", [frequency + 10000]);
				}
			}, function(e){
			}, "Compass", "getTimeout", []);

			// Start watch timer to get headings
			var id = Cordova.createUUID();
			navigator.compass.timers[id] = setInterval(function(){
				Cordova.exec(successCallback, errorCallback, "Compass", "getCurrentHeading", []);
			}, (frequency ? frequency : 1));

			return id;
		};

		/**
		 * Clears the specified heading watch.
		 * 
		 * @param {String}
		 *            id The ID of the watch returned from #watchHeading.
		 */
		Compass.prototype.clearWatch = function(id){

			// Stop javascript timer & remove from timer list
			if (id && navigator.compass.timers[id]) {
				clearInterval(navigator.compass.timers[id]);
				delete navigator.compass.timers[id];
			}
		};

		Compass.prototype.watchHeadingFilter = function(successCallback, errorCallback, options){
			var filter = (options && options.filter) ? options.filter : 10;
			var frequency = (options && options.frequency) ? options.frequency : 1000;

			// successCallback required
			if (typeof successCallback !== "function") {
				_consoleLog("Compass Error: successCallback is not a function");
				return;
			}

			// errorCallback optional
			if (errorCallback && (typeof errorCallback !== "function")) {
				_consoleLog("Compass Error: errorCallback is not a function");
				return;
			}

			// Make sure compass timeout > frequency + 10 sec
			Cordova.exec(function(timeout){
				if (timeout < (frequency + 10000)) {
					Cordova.exec(null, null, "Compass", "setTimeout", [frequency + 10000]);
				}
			}, function(e){
			}, "Compass", "getTimeout", []);

			var lastHeading = undefined;
			this.getCurrentHeading(function(compassHeading){
				lastHeading = compassHeading.magneticHeading;
			}, function(e){

			}, []);

			// Start watch timer to get headings
			var id = Cordova.createUUID();
			var that = this;
			navigator.compass.timers[id] = setInterval(function(){
				that.getCurrentHeading(function(compassHeading){
					if (lastHeading != undefined) {
						var dh = compassHeading.magneticHeading - lastHeading;
						if (dh < -360)
							dh += 360;
						if (dh > 360)
							dh -= 360;
						if (Math.abs(dh) >= filter) {
							Cordova.exec(successCallback, errorCallback, "Compass", "getCurrentHeading", []);
							lastHeading = compassHeading.magneticHeading;
						}
					} else {
						lastHeading = compassHeading.magneticHeading;
					}
				}, function(e){

				}, []);

			}, frequency);
			return id;
		};

		Compass.prototype.clearWatchFilter = function(id){
			if (id && navigator.compass.timers[id]) {
				clearInterval(navigator.compass.timers[id]);
				delete navigator.compass.timers[id];
			}
		};

		Cordova.addConstructor(function(){
			if (typeof navigator.compass === "undefined") {
				navigator.compass = new Compass();
			}
		});
	}());
}
